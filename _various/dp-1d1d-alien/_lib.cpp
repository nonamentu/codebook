constexpr int64_t kInf = 1e16;
constexpr int kN = 3e5;
int n, k;
int64_t a[kN], s[kN], dp[kN], val[kN];
void Pre() {
  cin >> n >> k;
  s[0] = a[0] = 0;
  for (int i = 1; i <= n; i++) {
    cin >> a[i];
    s[i] = s[i - 1] + a[i];
  }
}
int64_t Value(int l, int r) {
  int mid = (l + r + 1) >> 1;
  int64_t ans = ((mid << 1) - r - l) * a[mid] +
    s[r] + s[l] - (s[mid] << 1);
  return ans;
}
pair<int64_t, int> F(int l, int r) {
  return make_pair(dp[l] + Value(l, r), val[l]);
}
struct P {
  int i, l, r;
};
pair<int64_t, int> Aliens(int64_t cost) {
  dp[0] = val[0] = 0;
  deque<P> dq;
  dq.push_back({0, 1, n});
  for (int i = 1; i <= n; i++) {
    P ii = dq.front();
    dp[i] = dp[ii.i] + Value(ii.i, i) + cost;
    val[i] = val[ii.i] + 1;
    while (!dq.empty() && dq.front().r < i + 1)
      dq.pop_front();
    dq.front().l = i + 1;
    while (!dq.empty() &&
      F(dq.back().i, dq.back().l) >
        F(i, dq.back().l))
      dq.pop_back();
    P tmp = {i, i + 1, n};
    if (!dq.empty()) {
      int now = dq.back().l;
      for (int j = 20; j >= 0; j--)
        if (now + (1 << j) <= dq.back().r) {
          if (F(i, now + (1 << j)) >
            F(dq.back().i, now + (1 << j)))
            now += (1 << j);
        }
      dq.back().r = now, tmp.l = now + 1;
    }
    if (tmp.l <= n) dq.push_back(tmp);
  }
  return make_pair(dp[n], val[n]);
}
int main() {
  Pre();
  if (n == k) {
    cout << "0\n";
    return 0;
  }
  int64_t now = 0, tot = kInf;
  for (int i = 60; i >= 0; i--)
    if (now + (1LL << i) <= tot) {
      pair<int64_t, int> tmp =
        Aliens(now + (1LL << i));
      if (tmp.second == k) {
        cout << tmp.first - (now + (1LL << i)) * k
             << '\n';
        return 0;
      }
      if (tmp.second > k) now += (1LL << i);
    }
  ++now;
  pair<int64_t, int> ans = Aliens(now);
  cout << ans.first - now * k << '\n';
}
