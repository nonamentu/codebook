#include <bits/stdc++.h>
using namespace std;
#include "../misc/_lib.cpp"
int LogMod(int a, int b, int n) {
  int m = static_cast<int>(sqrt(n + 0.5)),
      v =
        static_cast<int>(Inv(PowMod(a, m, n), n));
  unordered_map<int, int> x;
  x[1] = 0;
  for (int i = 1, e = 1; i < m; i++) {
    e = static_cast<int>(MulMod(e, a, n));
    if (!x.count(e)) x[e] = i;
  }
  for (int i = 0; i < m; i++) {
    if (x.count(b)) return i * m + x[b];
    b = static_cast<int>(MulMod(b, v, n));
  }
  return -1;
}
int main() {
  cin.tie(nullptr),
    ios_base::sync_with_stdio(false);
  int n = 1000;
  mt19937 rng(random_device{}());
  for (int i = 0; i < n; i++) {
    int x = rng() % (n - 1);
    int pw = static_cast<int>(PowMod(3, x, kMod));
    assert(LogMod(3, pw, kMod) == x);
    cerr << '.';
  }
}
