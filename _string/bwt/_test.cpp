#include <bits/stdc++.h>
using namespace std;
#include "_lib.cpp"
int main() {
  cin.tie(nullptr),
    ios_base::sync_with_stdio(false);
  mt19937 rng(random_device{}());
  string s;
  for (int i = 0; i < kN / 2 - 1; i++) {
    s.push_back(
      static_cast<char>(kBase + rng() % kSigma));
  }
  string bwted = s + "0", ibwted = s + "0";
  BurrowsWheeler::BWT(s.data(), bwted.data());
  BurrowsWheeler::IBWT(
    bwted.data(), ibwted.data());
  auto idx = ibwted.find('{');
  ibwted =
    ibwted.substr(idx + 1) + ibwted.substr(0, idx);
  assert(s == ibwted);
}
