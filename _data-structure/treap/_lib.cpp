uint32_t Rng() {
  static uint32_t x = 71227123, y = 88030166;
  return y = x + y, x = y - x;
}
struct Treap {
  int data{}, sz = 1;
  array<Treap *, 2> ch{};
  Treap *fa{};
  Treap() : Treap(0) {}
  explicit Treap(int d) : data(d) {}
  friend int size(Treap *t) {
    return t ? t->sz : 0;
  }
  void Push() {}
  void Pull() {
    sz = 1;
    for (Treap *k : ch) {
      if (k) {
        sz += size(k);
        k->fa = this;
      }
    }
  }
};
Treap *Join(Treap *l, Treap *r) {
  if (!l || !r) return l ? l : r;
  int64_t sl = size(l), sr = size(r);
  if (Rng() * (sl + sr) <= sl * UINT_MAX) {
    l->Push();
    l->ch[1] = Join(l->ch[1], r);
    l->Pull();
    return l;
  }
  r->Push();
  r->ch[0] = Join(l, r->ch[0]);
  r->Pull();
  return r;
}
pair<Treap *, Treap *> SplitSize(
  Treap *t, int left) {
  if (t) t->fa = nullptr;
  if (size(t) <= left) return {t, nullptr};
  t->Push();
  Treap *a = nullptr, *b = nullptr;
  int sl = size(t->ch[0]) + 1;
  if (sl <= left) {
    a = t;
    tie(a->ch[1], b) =
      SplitSize(t->ch[1], left - sl);
  } else {
    b = t;
    tie(a, b->ch[0]) = SplitSize(t->ch[0], left);
  }
  t->Pull();
  return {a, b};
}
Treap *Find(Treap *t) {
  return t->fa ? Find(t->fa) : t;
}
int Rank(Treap *t) {
  int ret = 0;
  Treap *r = nullptr;
  while (t) {
    if (!r || t->ch[1] == r) {
      ret += size(t->ch[0]) + 1;
    }
    r = t;
    t = t->fa;
  }
  return ret;
}
void SplitAt(
  uint32_t p, Treap *n, Treap *&a, Treap *&b);
int GetMin(Treap *n);
// Merge two treaps, amortized log^2(n)
Treap *Merge(Treap *a, Treap *b) {
  if (!a || !b) return a ? a : b;
  int min_a = GetMin(a), min_b = GetMin(b);
  if (min_a > min_b) swap(a, b);
  Treap *tmp = nullptr;
  SplitAt(min_b, a, a, tmp);
  return Merge(Join(a, b), tmp);
}
