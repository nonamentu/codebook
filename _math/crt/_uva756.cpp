#include <bits/stdc++.h>
using namespace std;
constexpr int64_t kMod = int64_t(1E9 + 7);
constexpr double kEps = 1E-10;
int64_t PowMod(
  int64_t x, int64_t e, int64_t m = kMod) {
  if (e == 0) return 1;
  __int128 t = PowMod(x, e / 2, m);
  t = t * t % m;
  if (e % 2 == 1) t = t * x % m;
  return int64_t(t);
}
int64_t Inv(int64_t x, int64_t p = kMod) {
  return PowMod(x, p - 2);
}
int64_t MulMod(
  int64_t x, int64_t y, int64_t m = kMod) {
  return x * y % m;
}
int64_t AddMod(
  int64_t x, int64_t y, int64_t m = kMod) {
  return (x + y) % m;
}
template <typename T>
tuple<T, T, T> ExtGcd(T a, T b) {
  // (d, x, y): ax + by = d, d = gcd(a, b)
  if (!b) return make_tuple(a, 1, 0);
  T d, x, y;
  tie(d, x, y) = ExtGcd(b, a % b);
  return make_tuple(d, y, x - (a / b) * y);
}
int64_t Crt(int n, int64_t *m, int *a) {
  int64_t mod = 1, x = 0;
  for (int i = 0; i < n; i++) mod *= m[i];
  for (int i = 0; i < n; i++) {
    int64_t w = mod / m[i];
    auto t = ExtGcd(m[i], w);
    x = (x + get<2>(t) * w * a[i]) % mod;
  }
  return (x + mod) % mod;
}
int main() {
  cin.tie(nullptr),
    ios_base::sync_with_stdio(false);
  int p{}, e{}, i{}, d{};
  int cases = 0;
  while (cin >> p >> e >> i >> d && p != -1) {
    int64_t m[] = {23, 28, 33};
    int a[] = {p, e, i};
    int mod = 23 * 28 * 33;
    auto dd = Crt(3, m, a);
    if (!dd) dd = mod;
    dd = (dd - d + mod) % mod;
    if (!dd) dd = mod;
    cout << "Case " << ++cases
         << ": the next triple peak occurs in "
         << dd << " days.\n";
  }
}
