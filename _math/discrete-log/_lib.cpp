int LogMod(int a, int b, int n) {
  int m = static_cast<int>(sqrt(n + 0.5)),
      v =
        static_cast<int>(Inv(PowMod(a, m, n), n));
  unordered_map<int, int> x;
  x[1] = 0;
  for (int i = 1, e = 1; i < m; i++) {
    e = static_cast<int>(MulMod(e, a, n));
    if (!x.count(e)) x[e] = i;
  }
  for (int i = 0; i < m; i++) {
    if (x.count(b)) return i * m + x[b];
    b = static_cast<int>(MulMod(b, v, n));
  }
  return -1;
}
