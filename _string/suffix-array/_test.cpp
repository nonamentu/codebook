#include <bits/stdc++.h>
using namespace std;
#include "_lib.cpp"
mt19937 rng(random_device{}());
void Test(const string &str) {
  int n = static_cast<int>(str.size());
  Build(str);
  for (int i = 1; i < n; i++) {
    auto suf1 = str.substr(sa[i - 1]);
    auto suf2 = str.substr(sa[i]);
    assert(suf1 < suf2);
    size_t j = 0;
    for (; j < min(suf1.size(), suf2.size());
         j++) {
      if (suf1[j] != suf2[j]) break;
    }
    assert(static_cast<int>(j) == hi[i]);
  }
}
int main() {
  cin.tie(nullptr),
    ios_base::sync_with_stdio(false);
  string str;
  int n = 10000;
  for (int i = 0; i < n; i++) {
    str.push_back(static_cast<char>(rng() % 128));
  }
  Test(str);
  char c = str[0];
  str.clear();
  for (int i = 0; i < n; i++) { str.push_back(c); }
  Test(str);
}
