#include <bits/stdc++.h>
using namespace std;
#include "_lib.cpp"
int main() {
  cin.tie(nullptr),
    ios_base::sync_with_stdio(false);
  int n{}, m{};
  cin >> n >> m;
  for (int i = 0; i < m; i++) {
    int u{}, v{}, f{};
    cin >> u >> v >> f;
    if (u == v) continue;
    AddEdge(u, v, f);
    AddEdge(v, u, f);
  }
  cout << Dinic(1, n) << '\n';
}
