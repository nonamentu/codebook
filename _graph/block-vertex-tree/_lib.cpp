constexpr int kN = 100010;
vector<int> g[kN], bcc_g[kN];
stack<int> stk;
int in[kN], dfs_clock;
int bcc_cnt,
  rep[kN];  // rep[id]: the top of bcc component id
vector<int> bcc[kN];
int sz[kN],
  circle_id[kN];  // circle_id[x]: position of x on
                  // the circle, only needed when
                  // the graph is cactus
bool iscut[kN];
int idx[kN], cut[kN];
int Dfs(int u, int fa = 0) {
  int lowu = in[u] = ++dfs_clock;
  stk.push(u);
  for (int v : g[u]) {
    if (v != fa) {
      if (in[v]) {
        lowu = min(lowu, in[v]);
      } else {
        int lowv = Dfs(v, u);
        lowu = min(lowu, lowv);
        if (lowv >= in[u]) {
          iscut[u] |= (in[u] > 1 || in[v] > 2);
          bcc_cnt++;
          int cur = 0;
          bcc[bcc_cnt].push_back(u), cur++;
          rep[bcc_cnt] = u;
          while (bcc[bcc_cnt].back() != v) {
            int x = stk.top();
            bcc[bcc_cnt].push_back(x),
              circle_id[x] = cur++;
            stk.pop();
          }
          sz[bcc_cnt] =
            static_cast<int>(bcc[bcc_cnt].size());
        }
      }
    }
  }
  return lowu;
}
int Build(int n) {
  int tree_n = bcc_cnt;
  for (int i = 1; i <= n; i++)
    if (iscut[i])
      idx[i] = ++tree_n, cut[tree_n] = i;
  for (int i = 1; i <= bcc_cnt; i++) {
    for (int x : bcc[i]) {
      if (iscut[x])
        bcc_g[idx[x]].push_back(i),
          bcc_g[i].push_back(idx[x]);
      else
        idx[x] = i;
    }
  }
  return tree_n;
}
bool IsBlock(int x) { return x > bcc_cnt; }
// block-vertex-tree is stored in bcc_g
