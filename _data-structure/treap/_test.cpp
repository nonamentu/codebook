#include <bits/stdc++.h>
using namespace std;
#include "_lib.cpp"
Treap pool[1 << 20], *head = pool;
int main() {
  cin.tie(nullptr),
    ios_base::sync_with_stdio(false);
  int n = 1000000;
  mt19937 rng(random_device{}());
  Treap *root = nullptr;
  for (int i = 0; i < n; i++) {
    root = Join(root, new (head++) Treap(i));
  }
  for (int i = 0; i < n; i++) {
    int p = rng() % n;
    auto [l, r] = SplitSize(root, p);
    assert(size(l) == p);
    assert(size(r) == n - p);
    root = Join(r, l);
  }
}
