#include <bits/stdc++.h>
using namespace std;
#include "_lib.cpp"
int main() {
  cin.tie(nullptr),
    ios_base::sync_with_stdio(false);
  int n{}, m{};
  cin >> n >> m;
  matching::Init(n + 1);
  for (int i = 0; i < m; i++) {
    int u{}, v{};
    cin >> u >> v;
    matching::g[u].push_back(v);
    matching::g[v].push_back(u);
  }
  cout << matching::Solve(n + 1) << '\n';
  for (int x = 1; x <= n; x++) {
    if (matching::match[x] == n + 1)
      matching::match[x] = 0;
  }
  for (int x = 1; x <= n; x++)
    cout << matching::match[x]
         << (x == n ? "\n" : " ");
}
