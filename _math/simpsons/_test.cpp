#include <bits/stdc++.h>
using namespace std;
#include "../misc/_lib.cpp"
#include "_lib.cpp"
int main() {
  cin.tie(nullptr),
    ios_base::sync_with_stdio(false);
  cout << Quad(0, 1, [](double x) {
    return 4 * x * x * x;
  }) << '\n';
  cout << Quad(0, 1, [](double x) {
    return 3 * x * x;
  }) << '\n';
  cout << Quad(0, 1, [](double x) {
    return 2 * x;
  }) << '\n';
}
