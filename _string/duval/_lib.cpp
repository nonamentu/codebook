vector<pair<string, int>> Duval(const string &s) {
  int n = static_cast<int>(s.size()), i = 0;
  vector<pair<string, int>> factorization;
  while (i < n) {
    int j = i + 1, k = i;
    while (j < n && s[k] <= s[j]) {
      if (s[k] < s[j]) k = i;
      else
        k++;
      j++;
    }
    int ii = i;
    while (i <= k) {
      factorization.emplace_back(
        s.substr(i, j - k), ii);
      i += j - k;
    }
  }
  return factorization;
}
