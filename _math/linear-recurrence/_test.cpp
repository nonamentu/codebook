#include <bits/stdc++.h>
using namespace std;
#include "../misc/_lib.cpp"
#include "_lib.cpp"
int main() {
  cin.tie(nullptr),
    ios_base::sync_with_stdio(false);
  mt19937 rng(random_device{}());
  for (int t = 0; t < 1000; t++) {
    n = rng() % 100000;
    vector<int64_t> v_i{0, 1}, v_a{1, 1};
    int64_t res1 = Solve(v_i, v_a);
    int a = 0, b = 1;
    for (int i = 0; i < n; i++) {
      tie(a, b) = make_pair(b, (a + b) % kMod);
    }
    assert(res1 == b);
    cerr << '.';
  }
}
