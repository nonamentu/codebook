constexpr int kN = 1E5;
int64_t n, m, dp[kN + kN];
// dp[n] = \Sum_{i=0}^{m-1} A_i * dp[n - i - 1]
void PreDp(const vector<int64_t> &a) {
  dp[0] = 1;
  int64_t bdr = min(m + m, n);
  for (int64_t i = 1; i <= bdr; i++) {
    dp[i] = 0;
    for (int64_t j = i - 1;
         j >= max(int64_t(0), i - m); j--)
      dp[i] =
        AddMod(dp[i], MulMod(dp[j], a[i - j - 1]));
  }
}
vector<int64_t> Mul(const vector<int64_t> &v1,
  const vector<int64_t> &v2) {
  int sz1 = static_cast<int>(v1.size()),
      sz2 = static_cast<int>(v2.size());
  assert(sz1 == m), assert(sz2 == m);
  vector<int64_t> vv(m + m);
  for (int i = 0; i < m + m; i++) vv[i] = 0;
  for (int i = 0; i < sz1; i++)
    for (int j = 0; j < sz2; j++)
      vv[i + j + 1] = AddMod(
        vv[i + j + 1], MulMod(v1[i], v2[j]));
  for (int i = 0; i < m; i++)
    for (int j = 1; j <= m; j++)
      vv[i + j] = AddMod(vv[i + j], vv[i]);
  for (int i = 0; i < m; i++) vv[i] = vv[i + m];
  vv.resize(m);
  return vv;
}
int64_t Solve(
  vector<int64_t> &v_i, vector<int64_t> &v_a) {
  m = static_cast<int>(v_a.size());
  PreDp(v_a);
  if (n <= m + m) return dp[n];
  v_i.resize(m);
  for (int i = 0; i < m; i++) v_i[i] = 1;
  int64_t dlt = (n - m) / m, rdlt = dlt * m;
  while (dlt) {
    if (dlt & 1LL) v_i = Mul(v_i, v_a);
    v_a = Mul(v_a, v_a);
    dlt >>= 1;
  }
  int64_t ans = 0;
  for (int i = 0; i < m; i++)
    ans = AddMod(
      ans, MulMod(v_i[i], dp[n - i - 1 - rdlt]));
  return ans;
}
