#include <bits/stdc++.h>
using namespace std;
#include "_lib.cpp"
int main() {
  cin.tie(nullptr),
    ios_base::sync_with_stdio(false);
  int n{};
  cin >> n;
  while (n--) {
    string s;
    cin >> s;
    auto fact = Duval(s + s);
    size_t sz = 0;
    for (auto [ss, i] : fact) {
      if (sz + ss.size() >= s.size()) {
        cout << i + 1 << '\n';
        break;
      }
      sz += ss.size();
    }
  }
}
