#define S(a, b) \
  (f(a) + 4 * f((a + b) / 2) + f(b)) * (b - a) / 6
template <class F>
double Rec(
  double a, double b, F &f, double eps, double S) {
  double c = (a + b) / 2;
  double S1 = S(a, c), S2 = S(c, b), T = S1 + S2;
  if (abs(T - S) <= 15 * eps || b - a < 1e-10)
    return T + (T - S) / 15;
  return Rec(a, c, f, eps / 2, S1) +
    Rec(c, b, f, eps / 2, S2);
}
template <class F>
double Quad(double a, double b, F &&f) {
  return Rec(a, b, f, kEps, S(a, b));
}
