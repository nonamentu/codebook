#include <bitset>
#include <iostream>
#include <vector>
using namespace std;
#define nullptr NULL
const int kN = 510, kInf = 1000000010;
bitset<kN> went;
int edge[kN][kN];
int Merge(int n, int &s, int &t) {
  vector<int> v;
  bitset<kN> used;
  int dis[kN];
  used.reset();
  for (int i = 0; i < n; i++)
    if (!went[i]) v.push_back(i);
  int tot = static_cast<int>(v.size());
  dis[0] = 0;
  for (int i = 1; i < tot; i++)
    dis[i] = edge[v[0]][v[i]];
  used[0] = true;
  for (int i = 2; i < tot; i++) {
    s = 0;
    for (int j = 1; j < tot; j++)
      if (!used[j])
        if (dis[j] > dis[s]) s = j;
    for (int j = 1; j < tot; j++)
      dis[j] += edge[v[s]][v[j]];
    used[s] = true;
  }
  for (int i = 0; i < tot; i++) {
    if (!used[i]) {
      t = i;
      break;
    }
  }
  int ans = dis[t];
  s = v[s];
  t = v[t];
  return ans;
}
int Solve(int n) {
  int s = -1, t = -1, ans = kInf, cnt = n;
  went.reset();
  while (cnt > 2) {
    ans = min(ans, Merge(n, s, t));
    went[t] = true;
    for (int i = 0; i < n; i++) {
      edge[s][i] += edge[t][i];
      edge[i][s] += edge[i][t];
      edge[t][i] = edge[i][t] = 0;
    }
    --cnt;
  }
  for (int i = 0; i < n; i++) {
    if (!went[i]) {
      s = i;
      break;
    }
  }
  for (int i = s + 1; i < n; i++) {
    if (!went[i]) {
      t = i;
      break;
    }
  }
  return min(ans, edge[s][t]);
}
int main() {
  cin.tie(nullptr),
    ios_base::sync_with_stdio(false);
  int n{}, m{};
  while (cin >> n >> m) {
    for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++) edge[i][j] = 0;
    for (int i = 0; i < m; i++) {
      int u{}, v{}, c{};
      cin >> u >> v >> c;
      edge[u][v] += c;
      edge[v][u] += c;
    }
    cout << Solve(n) << '\n';
  }
}
