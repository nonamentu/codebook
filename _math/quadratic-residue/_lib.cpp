bool Check(int x, int p) {
  return PowMod(x, (p - 1) >> 1, p) == 1;
}
int QuadraticResidue(int n, int p) {
  n %= p;
  if (n < 0) n += p;
  if (!Check(n, p)) return -1;
  int z = -1, s = 0;
  for (z = 2;; z++)
    if (!Check(z, p)) break;
  assert(z != -1);
  while ((p - 1) % (1 << s) == 0) s++;
  s--;
  int q = (p - 1) / (1 << s);
  if (s == 1) {
    return static_cast<int>(
      PowMod(n, (p + 1) >> 2, p));
  }
  int c = static_cast<int>(PowMod(z, q, p));
  int r =
    static_cast<int>(PowMod(n, (q + 1) >> 1, p));
  int t = static_cast<int>(PowMod(n, q, p));
  int m = s;
  for (;;) {
    if (t == 1) return r;
    int i = 0, zz = t;
    while (zz != 1 && i < m - 1) {
      zz = static_cast<int>(1LL * zz * zz % p);
      ++i;
    }
    int b = c, e = m - i - 1;
    while (e--)
      b = static_cast<int>(1LL * b * b % p);
    r = static_cast<int>(1LL * r * b % p);
    c = static_cast<int>(1LL * b * b % p);
    t = static_cast<int>(1LL * t * c % p);
    m = i;
  }
}
