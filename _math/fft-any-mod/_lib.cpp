constexpr int kN = 131072;
const double kPi = acos(-1);
using Cp = complex<double>;
struct FFT {
  int n{}, rev[kN] = {};
  Cp omega[kN], iomega[kN];
  void Init(int n_) {
    this->n = n_;
    for (int i = 0; i < n_; i++) {
      omega[i] = Cp(cos(2 * kPi / n_ * i),
        sin(2 * kPi / n_ * i));
      iomega[i] = conj(omega[i]);
    }
    int k = __lg(n_);
    for (int i = 0; i < n_; i++) {
      int t = 0;
      for (int j = 0; j < k; j++)
        if (i & (1 << j)) t |= (1 << (k - j - 1));
      rev[i] = t;
    }
  }
  void Transform(vector<Cp> &a, Cp *xomega) {
    for (int i = 0; i < n; i++)
      if (i < rev[i]) swap(a[i], a[rev[i]]);
    for (int len = 2; len <= n; len <<= 1) {
      int mid = len >> 1;
      int r = n / len;
      for (int j = 0; j < n; j += len)
        for (int i = 0; i < mid; i++) {
          Cp t = xomega[r * i] * a[j + mid + i];
          a[j + mid + i] = a[j + i] - t;
          a[j + i] += t;
        }
    }
  }
  void Fft(vector<Cp> &a) { Transform(a, omega); }
  void IFft(vector<Cp> &a) {
    Transform(a, iomega);
    for (int i = 0; i < n; i++) a[i] /= n;
  }
};
vector<int> Conv(const vector<int> &a,
  const vector<int> &b, int p) {
  int sz = 1;
  while (sz < static_cast<int>(a.size()) +
      static_cast<int>(b.size()) - 1)
    sz <<= 1;
  vector<Cp> fa(sz), fb(sz);
  for (int i = 0; i < static_cast<int>(a.size());
       ++i)
    fa[i] = Cp(a[i] & ((1 << 15) - 1), a[i] >> 15);
  for (int i = 0; i < static_cast<int>(b.size());
       ++i)
    fb[i] = Cp(b[i] & ((1 << 15) - 1), b[i] >> 15);
  FFT solver;
  solver.Init(sz);
  solver.Fft(fa), solver.Fft(fb);
  double r = 0.25 / sz;
  Cp r2(0, -1), r3(r, 0), r4(0, -r), r5(0, 1);
  for (int i = 0; i <= (sz >> 1); ++i) {
    int j = (sz - i) & (sz - 1);
    Cp a1 = (fa[i] + conj(fa[j]));
    Cp a2 = (fa[i] - conj(fa[j])) * r2;
    Cp b1 = (fb[i] + conj(fb[j])) * r3;
    Cp b2 = (fb[i] - conj(fb[j])) * r4;
    if (i != j) {
      Cp c1 = (fa[j] + conj(fa[i]));
      Cp c2 = (fa[j] - conj(fa[i])) * r2;
      Cp d1 = (fb[j] + conj(fb[i])) * r3;
      Cp d2 = (fb[j] - conj(fb[i])) * r4;
      fa[i] = c1 * d1 + c2 * d2 * r5;
      fb[i] = c1 * d2 + c2 * d1;
    }
    fa[j] = a1 * b1 + a2 * b2 * r5;
    fb[j] = a1 * b2 + a2 * b1;
  }
  solver.Fft(fa), solver.Fft(fb);
  vector<int> res(sz);
  for (int i = 0; i < sz; ++i) {
    auto x = int64_t(round(fa[i].real()));
    auto y = int64_t(round(fb[i].real()));
    auto z = int64_t(round(fa[i].imag()));
    res[i] = static_cast<int>(
      (x + ((y % p) << 15) + ((z % p) << 30)) % p);
  }
  return res;
}
vector<int64_t> Mul(
  vector<int64_t> a, vector<int64_t> b) {
  int n = 1;
  int n1 = static_cast<int>(a.size()),
      n2 = static_cast<int>(b.size());
  while (n < n1 + n2) n <<= 1;
  vector<Cp> tmp_a(n), tmp_b(n);
  // MAKE SURE THAT THE PRECISION IS OK
  for (int i = 0; i < n1; i++)
    tmp_a[i] = Cp(static_cast<double>(a[i]), 0);
  for (int i = 0; i < n2; i++)
    tmp_b[i] = Cp(static_cast<double>(b[i]), 0);
  FFT solver;
  solver.Init(n);
  solver.Fft(tmp_a), solver.Fft(tmp_b);
  for (int i = 0; i < n; i++) tmp_a[i] *= tmp_b[i];
  solver.IFft(tmp_a);
  vector<int64_t> ans(n);
  for (int i = 0; i < n; i++)
    ans[i] = int64_t(round(tmp_a[i].real()));
  return ans;
}
