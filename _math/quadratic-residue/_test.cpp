#include <bits/stdc++.h>
using namespace std;
#include "../misc/_lib.cpp"
#include "_lib.cpp"
int main() {
  cin.tie(nullptr),
    ios_base::sync_with_stdio(false);
  constexpr int kM = 65537;
  for (int i = 1; i < kM; i++) {
    int x = static_cast<int>(1LL * i * i % kM);
    int res = QuadraticResidue(x, kM);
    assert(
      static_cast<int>(1LL * res * res % kM) == x);
  }
}
