#include "_string/suffix-array/_lib.cpp"
constexpr int kSigma = 26;
constexpr char kBase = 'a';
namespace BurrowsWheeler {
void BWT(const char *ori, char *res) {
  string s(ori);
  s.push_back(kBase + kSigma);
  Build(s + s);
  char *p = res;
  for (size_t i = 0; i < s.size() * 2; i++) {
    if (sa[i] < static_cast<int>(s.size())) {
      *p++ = sa[i] ? s[sa[i] - 1] : s.back();
    }
  }
}
// Result needs to be rotated
void IBWT(char *ori, char *res) {
  vector<int> v[kSigma + 1];
  int len = static_cast<int>(strlen(ori));
  for (int i = 0; i < len; i++)
    v[ori[i] - kBase].push_back(i);
  vector<int> a;
  for (int i = 0, ptr = 0; i < kSigma + 1; i++)
    for (auto j : v[i]) {
      a.push_back(j);
      ori[ptr++] = static_cast<char>(kBase + i);
    }
  for (int i = 0, ptr = 0; i < len; i++) {
    res[i] = ori[a[ptr]];
    ptr = a[ptr];
  }
  res[len] = 0;
}
};  // namespace BurrowsWheeler
