#include "_math/fft-ntt-ltf0501/_lib.cpp"
#include "_math/quadratic-residue/_lib.cpp"
NTT ntt;
// First n elements
void Ntt(vector<int> &vec, int n) {
  ntt.Init(n, 3), ntt.Dft(vec);
}
void INtt(vector<int> &vec, int n) {
  ntt.Init(n, 3), ntt.IDft(vec);
}
vector<int> Inverse(const vector<int> &v, int n) {
  vector<int> q(
    1, static_cast<int>(PowMod(v[0], kMod - 2)));
  for (int i = 2; i <= n; i <<= 1) {
    vector<int> fv(v.begin(), v.begin() + i);
    vector<int> fq(q.begin(), q.end());
    fv.resize(2 * i), fq.resize(2 * i);
    Ntt(fq, 2 * i), Ntt(fv, 2 * i);
    for (int j = 0; j < 2 * i; ++j) {
      fv[j] = static_cast<int>(
        1LL * fv[j] * fq[j] % kMod * fq[j] % kMod);
    }
    INtt(fv, 2 * i);
    vector<int> res(i);
    for (int j = 0; j < i; ++j) {
      res[j] = kMod - fv[j];
      if (j < (i >> 1))
        (res[j] += 2 * q[j] % kMod) %= kMod;
    }
    q = res;
  }
  return q;
}
vector<int> Divide(
  const vector<int> &a, const vector<int> &b) {
  // leading zero should be trimmed
  int n = static_cast<int>(a.size()),
      m = static_cast<int>(b.size());
  int k = 2;
  while (k < n - m + 1) k <<= 1;
  vector<int> ra(k), rb(k);
  for (int i = 0; i < min(n, k); ++i)
    ra[i] = a[n - i - 1];
  for (int i = 0; i < min(m, k); ++i)
    rb[i] = b[m - i - 1];
  vector<int> rbi = Inverse(rb, k);
  vector<int> res = Mul(rbi, ra);
  res.resize(n - m + 1);
  reverse(res.begin(), res.end());
  return res;
}
constexpr int kInv2 = PowMod(2, kMod - 2);
vector<int> Sqrt(vector<int> b, int n) {
  if (n == 1)
    return vector<int>{
      QuadraticResidue(b[0], kMod)};
  vector<int> h = Sqrt(b, n >> 1);
  h.resize(n);
  vector<int> c = Inverse(h, n);
  h.resize(n << 1);
  c.resize(n << 1);
  vector<int> res(n << 1);
  Ntt(h, n << 1);
  b.resize(n << 1);
  for (int i = n; i < (n << 1); ++i) b[i] = 0;
  Ntt(b, n << 1);
  Ntt(c, n << 1);
  for (int i = 0; i < (n << 1); ++i)
    res[i] = static_cast<int>(
      (h[i] + 1LL * c[i] * b[i] % kMod) % kMod *
      kInv2 % kMod);
  INtt(res, n << 1);
  for (int i = n; i < (n << 1); ++i) res[i] = 0;
  return res;
}
