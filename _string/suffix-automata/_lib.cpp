struct State {
  static constexpr int kSigma = 26;
  int len{}, link = -1, firstpos{};
  array<int, kSigma> trans{};
  State() : State(0) {}
  explicit State(int len_)
      : len(len_), firstpos(len_ - 1) {
    trans.fill(-1);
  }
  State(int len_, const State &src)
      : len(len_), link(src.link),
        firstpos(src.firstpos), trans(src.trans) {}
  [[nodiscard]] int &Trans(char c) {
    return trans[c - 'a'];
  }
};
using Sa = pair<vector<State>, int>;
Sa SaFactory() { return {vector<State>{{}}, 0}; }
void SaExtend(char c, Sa &sa) {
  auto &[sa_vec, p] = sa;
  sa_vec.emplace_back(sa_vec[p].len + 1);
  int cur = static_cast<int>(sa_vec.size()) - 1;
  for (; p != -1 && sa_vec[p].Trans(c) == -1;
       p = sa_vec[p].link) {
    sa_vec[p].Trans(c) = cur;
  }
  if (p == -1) {
    sa_vec[cur].link = 0;
  } else if (int q = sa_vec[p].Trans(c);
             sa_vec[p].len + 1 == sa_vec[q].len) {
    sa_vec[cur].link = q;
  } else {
    sa_vec.emplace_back(
      sa_vec[p].len + 1, sa_vec[q]);
    int clone =
      static_cast<int>(sa_vec.size() - 1);
    for (; p != -1 && sa_vec[p].Trans(c) == q;
         p = sa_vec[p].link) {
      sa_vec[p].Trans(c) = clone;
    }
    sa_vec[q].link = sa_vec[cur].link = clone;
  }
  p = cur;
}
