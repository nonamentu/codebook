#include <bits/stdc++.h>
using namespace std;
constexpr double kEps = 1e-9;
#include "_lib.cpp"
int main() {
  cin.tie(nullptr),
    ios_base::sync_with_stdio(false);
  int t{};
  cin >> t;
  while (t--) {
    Edge::head = Edge::pool;
    int n{};
    cin >> n;
    vector<P3> pts(n);
    for (auto &pt : pts)
      cin >> pt.x >> pt.y >> pt.z;
    auto fs = Hull3D(pts);
    vector<int> hull;
    for (auto &f : fs) {
      hull.push_back(f.a);
      hull.push_back(f.b);
      hull.push_back(f.c);
    }
    sort(hull.begin(), hull.end());
    hull.erase(unique(hull.begin(), hull.end()),
      hull.end());
    F x = 0, y = 0, z = 0;
    for (int i : hull) {
      x += pts[i].x;
      y += pts[i].y;
      z += pts[i].z;
    }
    x /= static_cast<F>(hull.size());
    y /= static_cast<F>(hull.size());
    z /= static_cast<F>(hull.size());
    P3 d(x, y, z);
    F area = 0, volume = 0;
    for (auto &f : fs) {
      auto &a = pts[f.a];
      auto &b = pts[f.b];
      auto &c = pts[f.c];
      area += (b - a).Cross(c - a).abs();
      volume +=
        abs((d - a).Dot((b - a).Cross(c - a)));
    }
    cout << fixed << setprecision(3) << area / 2
         << ' ' << volume / 6 << '\n';
  }
}
