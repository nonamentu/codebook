// kN = #(vertices)
// AddEdge, Dinic(source, sink) => flow
// Init: graph
// 0-based
constexpr int kN = 5010,
              kInf = static_cast<int>(1E9 + 10);
struct Edge {
  int to, rev, cap;
};
vector<Edge> graph[kN];
int dep[kN], iter[kN];
void AddEdge(int u, int v, int c) {
  graph[u].push_back(
    {v, static_cast<int>(graph[v].size()), c});
  graph[v].push_back(
    {u, static_cast<int>(graph[u].size()) - 1, 0});
}
bool Bfs(int s, int t, int lim) {
  memset(dep, -1, sizeof(dep));
  queue<int> q({s});
  iter[s] = dep[s] = 0;
  while (!q.empty()) {
    int now = q.front();
    q.pop();
    if (now == t) return true;
    for (Edge i : graph[now]) {
      if (i.cap >= lim && dep[i.to] == -1) {
        dep[i.to] = dep[now] + 1;
        iter[i.to] = 0;
        q.push(i.to);
      }
    }
  }
  return dep[t] != -1;
}
int Dfs(int u, int t, int nv) {
  if (u == t) return nv;
  for (int &i = iter[u];
       i < static_cast<int>(graph[u].size());
       i++) {
    Edge &nxt = graph[u][i];
    if (nxt.cap > 0 && dep[nxt.to] == dep[u] + 1) {
      if (int temp =
            Dfs(nxt.to, t, min(nxt.cap, nv))) {
        nxt.cap -= temp;
        graph[nxt.to][nxt.rev].cap += temp;
        return temp;
      }
    }
  }
  return 0;
}
int64_t Dinic(int s, int t) {
  int64_t ans = 0;
  for (int lim = 1 << 30; lim; lim >>= 1) {
    while (Bfs(s, t, lim)) {
      while (int f = Dfs(s, t, kInf)) ans += f;
    }
  }
  return ans;
}
